package co.com.proyectobase.screenplay.model;

public class Paciente {
	private String nombres;
	private String apellidos;
	private String telefono;
	private String tipoDocumento;
	private String numeroDocumento;
	private String saludPrepagada;
	
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public String getSaludPrepagada() {
		return saludPrepagada;
	}
	public void setSaludPrepagada(String saludPrepagada) {
		this.saludPrepagada = saludPrepagada;
	}

}
