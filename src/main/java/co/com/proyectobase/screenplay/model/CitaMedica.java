package co.com.proyectobase.screenplay.model;

public class CitaMedica {
	private String diaDeLaCita;
	private String documentoIdentidadPaciente;
	private String documentoIdentidadDoctor;
	private String observaciones;
	
	
	public String getDiaDeLaCita() {
		return diaDeLaCita;
	}
	public void setDiaDeLaCita(String diaDeLaCita) {
		this.diaDeLaCita = diaDeLaCita;
	}
	public String getDocumentoIdentidadPaciente() {
		return documentoIdentidadPaciente;
	}
	public void setDocumentoIdentidadPaciente(String documentoIdentidadPaciente) {
		this.documentoIdentidadPaciente = documentoIdentidadPaciente;
	}
	public String getDocumentoIdentidadDoctor() {
		return documentoIdentidadDoctor;
	}
	public void setDocumentoIdentidadDoctor(String documentoIdentidadDoctor) {
		this.documentoIdentidadDoctor = documentoIdentidadDoctor;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
}
