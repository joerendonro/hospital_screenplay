package co.com.proyectobase.screenplay.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class AgregarPacientePage extends PageObject {
	
	public static final Target NOMBRE =	Target.the("El campo nombre").located(By.xpath("//input[@name='name']"));
	
	public static final Target APELLIDOS = Target.the("El campo apellidos").located(By.xpath("//input[@name='last_name']"));
	
	public static final Target TELEFONO = Target.the("El telefono del paciente").located(By.xpath("//input[@name='telephone']"));
	
	public static final Target TIPO_DOCUMENTO = 
			Target.the("Tipo de documento de identidad  del paciente").located(By.xpath("//select[@name='identification_type']"));
	
	public static final Target NRO_DOCUMENTO = 
			Target.the("Numero de documento de identidad del paciente").located(By.xpath("//input[@name='identification']"));
	
	public static final Target CHECK_SALUD_PREPAGADA = Target.the("Salud prepagada del paciente").located(By.xpath("//input[@name='prepaid']"));

	public static final Target BTN_GUARDAR = Target.the("Guardar datos del paciente").located(By.xpath("//A[@onclick='submitForm()']"));
	
	public static final Target MSG_REGISTRO_EXITOSO = 
			Target.the("Botón guardar").located(By.xpath("//P[contains(text(), 'Datos guardados correctamente')]"));
}
