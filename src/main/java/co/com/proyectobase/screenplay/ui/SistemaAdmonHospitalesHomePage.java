package co.com.proyectobase.screenplay.ui;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://automatizacion.herokuapp.com/pperez/home")
public class SistemaAdmonHospitalesHomePage extends PageObject{
	
	public static final Target OPCION_AGREGAR_DOCTOR = 
			Target.the("Opción para registrar un doctor").located(By.xpath("//A[@href='addDoctor']"));
	
	public static final Target OPCION_AGREGAR_PACIENTE = 
			Target.the("Opción para registrar un Paciente").located(By.xpath("//A[@href='addPatient']"));
	
	public static final Target OPCION_AGENDAR_CITA = 
			Target.the("Opción para agendar una cita médica").located(By.xpath("//A[@href='appointmentScheduling']"));
	
}
