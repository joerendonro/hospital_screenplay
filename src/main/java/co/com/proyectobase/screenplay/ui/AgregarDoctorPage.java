package co.com.proyectobase.screenplay.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;


public class AgregarDoctorPage extends PageObject{
	public static final Target NOMBRE =	Target.the("El campo nombre").located(By.id("name"));
	
	public static final Target APELLIDOS =	Target.the("El campo apellidos").located(By.id("last_name"));
	
	public static final Target TELEFONO =	Target.the("El campo telefono").located(By.id("telephone"));
	
	public static final Target TIPO_DE_IDENTIFICACION =	
			Target.the("El campo tipo de identificacion").located(By.id("identification_type"));
	
	public static final Target NRO_DOCUMENTO =	
			Target.the("El campo número de documento").located(By.id("identification"));
	
	public static final Target BTN_GUARDAR = Target.the("Botón guardar").located(By.xpath("//A[@onclick='submitForm()']"));
	
	public static final Target MSG_REGISTRO_EXITOSO = 
			Target.the("Botón guardar").located(By.xpath("//P[contains(text(), 'Datos guardados correctamente')]"));
}
