package co.com.proyectobase.screenplay.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class AgendarCitaMedicaPage extends PageObject{
	
	public static final Target DIA_DE_LA_CITA = Target.the("Día de la cita médica").located(By.id("datepicker"));
	
	public static final Target NRO_DOCUMENTO_PACIENTE = 
			Target.the("Número de documento del paciente").located(By.xpath("(//INPUT[@type='text'])[2]"));
	
	public static final Target NRO_DOCUMENTO_DOCTOR = 
			Target.the("Número de documento del Doctor").located(By.xpath("(//INPUT[@type='text'])[3]"));
	
	public static final Target OBSERVACIONES = 
			Target.the("Observaciones sobre la cita").located(By.xpath("//TEXTAREA[@class='form-control']"));
	
	public static final Target BTN_AGENDAR_CITA = 
			Target.the("botón guardar cita medica").located(By.xpath("//A[@onclick='submitForm()'][text()='Guardar']"));
	
}
