package co.com.proyectobase.screenplay.questions;

import co.com.proyectobase.screenplay.ui.AgregarDoctorPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class ElResultado implements Question<String>{

	@Override
	public String answeredBy(Actor actor) {
		return Text.of(AgregarDoctorPage.MSG_REGISTRO_EXITOSO).viewedBy(actor).asString();
	}

	public static ElResultado es() {
		return new ElResultado();
	}

}
