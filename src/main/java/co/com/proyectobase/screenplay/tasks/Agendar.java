package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.model.CitaMedica;
import co.com.proyectobase.screenplay.ui.AgendarCitaMedicaPage;
import co.com.proyectobase.screenplay.ui.SistemaAdmonHospitalesHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class Agendar implements Task{
	
	private List<CitaMedica> datosDeLaCitaMedica;
	
	public Agendar(List<CitaMedica> datosCita) {
		datosDeLaCitaMedica = datosCita;
	}
	

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Click.on(SistemaAdmonHospitalesHomePage.OPCION_AGENDAR_CITA));
		
		actor.attemptsTo(Enter.theValue(datosDeLaCitaMedica.get(0).getDiaDeLaCita()).into(AgendarCitaMedicaPage.DIA_DE_LA_CITA));
		actor.attemptsTo(Click.on("//DIV[@class='panel-body']"));//Para ocultar el datepicker
		actor.attemptsTo(Enter.theValue(datosDeLaCitaMedica.get(0).getDocumentoIdentidadPaciente()).into(AgendarCitaMedicaPage.NRO_DOCUMENTO_PACIENTE));
		actor.attemptsTo(Enter.theValue(datosDeLaCitaMedica.get(0).getDocumentoIdentidadDoctor()).into(AgendarCitaMedicaPage.NRO_DOCUMENTO_DOCTOR));
		actor.attemptsTo(Enter.theValue(datosDeLaCitaMedica.get(0).getObservaciones()).into(AgendarCitaMedicaPage.OBSERVACIONES));
		
		actor.attemptsTo(Click.on(AgendarCitaMedicaPage.BTN_AGENDAR_CITA));
		
	}

	public static Agendar unaNuevaCitaMedica(List<CitaMedica> datosCita) {
		return Tasks.instrumented(Agendar.class, datosCita);
	}

}
