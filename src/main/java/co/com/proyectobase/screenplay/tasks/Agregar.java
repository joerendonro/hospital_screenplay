package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.model.Paciente;
import co.com.proyectobase.screenplay.ui.AgregarPacientePage;
import co.com.proyectobase.screenplay.ui.SistemaAdmonHospitalesHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

public class Agregar implements Task {
	
	private List<Paciente> datosPaciente;
	
	public Agregar(List<Paciente> datoPaciente) {
		this.datosPaciente = datoPaciente;
	}
	

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Click.on(SistemaAdmonHospitalesHomePage.OPCION_AGREGAR_PACIENTE));
		
		actor.attemptsTo(Enter.theValue(datosPaciente.get(0).getNombres()).into(AgregarPacientePage.NOMBRE));
		actor.attemptsTo(Enter.theValue(datosPaciente.get(0).getApellidos()).into(AgregarPacientePage.APELLIDOS));
		actor.attemptsTo(Enter.theValue(datosPaciente.get(0).getTelefono()).into(AgregarPacientePage.TELEFONO));
		actor.attemptsTo(SelectFromOptions.byVisibleText(datosPaciente.get(0).getTipoDocumento()).from(AgregarPacientePage.TIPO_DOCUMENTO));
		actor.attemptsTo(Enter.theValue(datosPaciente.get(0).getNumeroDocumento()).into(AgregarPacientePage.NRO_DOCUMENTO));
		
		if(datosPaciente.get(0).getSaludPrepagada().equals("Si"))
			actor.attemptsTo(Click.on(AgregarPacientePage.CHECK_SALUD_PREPAGADA));
		
		actor.attemptsTo(Click.on(AgregarPacientePage.BTN_GUARDAR));
		
	}

	public static Agregar unNuevoPaciente(List<Paciente> datosPaciente) {
		return Tasks.instrumented(Agregar.class, datosPaciente);
	}

}
