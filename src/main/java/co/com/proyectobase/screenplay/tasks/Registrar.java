package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.model.Doctor;
import co.com.proyectobase.screenplay.ui.AgregarDoctorPage;
import co.com.proyectobase.screenplay.ui.SistemaAdmonHospitalesHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

public class Registrar implements Task{
	
	private List<Doctor> datosDoctor;
	
	public Registrar(List<Doctor> datosDoctor) {
		this.datosDoctor = datosDoctor;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Click.on(SistemaAdmonHospitalesHomePage.OPCION_AGREGAR_DOCTOR));
		actor.attemptsTo(Enter.theValue(datosDoctor.get(0).getNombres()).into(AgregarDoctorPage.NOMBRE));
		actor.attemptsTo(Enter.theValue(datosDoctor.get(0).getApellidos()).into(AgregarDoctorPage.APELLIDOS));
		actor.attemptsTo(Enter.theValue(datosDoctor.get(0).getTelefono()).into(AgregarDoctorPage.TELEFONO));
		actor.attemptsTo(SelectFromOptions.byVisibleText(datosDoctor.get(0).getTipoDocumento()).from(AgregarDoctorPage.TIPO_DE_IDENTIFICACION));
		actor.attemptsTo(Enter.theValue(datosDoctor.get(0).getNumeroDocumento()).into(AgregarDoctorPage.NRO_DOCUMENTO));
		actor.attemptsTo(Click.on(AgregarDoctorPage.BTN_GUARDAR));
	}

	public static Registrar unNuevoDoctor(List<Doctor> datosDoctor) {
		return Tasks.instrumented(Registrar.class, datosDoctor);
	}

}
