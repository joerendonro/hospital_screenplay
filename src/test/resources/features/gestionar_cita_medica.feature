#Author: jerendon@bancolombia.com
# language:es
@gestionarCitaMedica
Característica: Sistema de administración de hospitales
  Como paciente
  Quiero realizar la solicitud de una cita médica
  A través del sistema de Administración de Hospitales

  @registrarDoctor
  Escenario: Realizar el Registro de un Doctor
    Dado que Elkin necesita registrar un nuevo doctor
    Cuando el realiza el registro del mismo en el aplicativo de Administración de Hospitales
      | nombres  | apellidos        | telefono | tipoDocumento        | numeroDocumento |
      | Jorge E. | Salazar Restrepo |  8743521 | Cédula de ciudadanía |         4356749 |
    Entonces el verifica que se presente en pantalla el mensaje Datos guardados correctamente.

  @registrarPaciente
  Escenario: Realizar el Registro de un Paciente
    Dado que Elkin necesita registrar un nuevo paciente
    Cuando el realiza el registro del paciente en el aplicativo de Administración de Hospitales
      | nombres | apellidos | telefono   | tipoDocumento        | numeroDocumento | saludPrepagada |
      | John    | Rendon R. | 3164570388 | Cédula de ciudadanía |        10897204 | Si             |
    Entonces el verifica que se presente en pantalla el mensaje Datos guardados correctamente.

  @solicitarCita
  Escenario: Realizar el Agendamiento de una Cita
    Dado que Elkin necesita asistir al medico
    Cuando el realiza el agendamiento de una Cita
      | diaDeLaCita | documentoIdentidadPaciente | documentoIdentidadDoctor | observaciones                                       |
      | 09/14/2018  |                   10897204 |                  4356749 | Cita prioritaria para atender Rinofaringitis aguda. |
    Entonces el verifica que se presente en pantalla el mensaje Datos guardados correctamente.
