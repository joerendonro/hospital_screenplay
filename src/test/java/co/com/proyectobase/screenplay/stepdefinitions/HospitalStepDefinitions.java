package co.com.proyectobase.screenplay.stepdefinitions;

import java.util.List;

import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.model.CitaMedica;
import co.com.proyectobase.screenplay.model.Doctor;
import co.com.proyectobase.screenplay.model.Paciente;
import co.com.proyectobase.screenplay.questions.ElResultado;
import co.com.proyectobase.screenplay.tasks.Abrir;
import co.com.proyectobase.screenplay.tasks.Agendar;
import co.com.proyectobase.screenplay.tasks.Agregar;
import co.com.proyectobase.screenplay.tasks.Registrar;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.equalTo;

public class HospitalStepDefinitions {
	
	@Managed(driver="chrome")
	private WebDriver hisBrowser;
	private Actor elkin = Actor.named("elkin");
	
	@Before
	public void configInit() {
		elkin.can(BrowseTheWeb.with(hisBrowser));
	}
	
	//@registrarDoctor
	@Dado("^que Elkin necesita registrar un nuevo doctor$")
	public void queElkinNecesitaRegistrarUnNuevoDoctor() {
		elkin.wasAbleTo(Abrir.laPaginaDelHospital());
	}

	@Cuando("^el realiza el registro del mismo en el aplicativo de Administración de Hospitales$")
	public void elRealizaElRegistroDelMismoEnElAplicativoDeAdministracionDeHospitales(List<Doctor> datosDoctor){
		System.out.println("Nombre del Doctor: "+datosDoctor.get(0).getNombres());
		elkin.attemptsTo(Registrar.unNuevoDoctor(datosDoctor));
	}
	
	
	//@registrarPaciente
	@Dado("^que Elkin necesita registrar un nuevo paciente$")
	public void queElkinNecesitaRegistrarUnNuevoPaciente() {
		elkin.wasAbleTo(Abrir.laPaginaDelHospital());
	}
	
	@Cuando("^el realiza el registro del paciente en el aplicativo de Administración de Hospitales$")
	public void elRealizaElRegistroDelPacienteEnElAplicativoDeAdministracionDeHospitales(List<Paciente> datosPaciente){
		System.out.println("Nombre del paciente: "+datosPaciente.get(0).getNombres());
		elkin.attemptsTo(Agregar.unNuevoPaciente(datosPaciente));
	}
	
	
	//@solicitarCita
	@Dado("^que Elkin necesita asistir al medico$")
	public void queElkinNecesitaAsistirAlMedico() {
		elkin.wasAbleTo(Abrir.laPaginaDelHospital());
	}
	
	@Cuando("^el realiza el agendamiento de una Cita$")
	public void elRealizaElAgendamientoDeUnaCita(List<CitaMedica> datosCita){
		System.out.println("Fecha de la cita: "+datosCita.get(0).getDiaDeLaCita());
		elkin.attemptsTo(Agendar.unaNuevaCitaMedica(datosCita));
	}
	
	
	//Paso compartido
	@Entonces("^el verifica que se presente en pantalla el mensaje (.*)$")
	public void elVerificaQueSePresenteEnPantallaElMensajeDatosGuardadosCorrectamente(String mensaje) {
		elkin.should(seeThat(ElResultado.es(), equalTo(mensaje)));
	}
	
}
